package com.payilagam.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payilagam.sms.entity.StudentEntity;

public interface Student_repository extends JpaRepository<StudentEntity, Integer> {

}
