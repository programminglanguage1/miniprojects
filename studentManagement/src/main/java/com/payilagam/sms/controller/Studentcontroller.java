package com.payilagam.sms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.payilagam.sms.entity.StudentEntity;
import com.payilagam.sms.repository.Student_repository;

@Controller
public class Studentcontroller {
	
	@RequestMapping("login") 
	public ModelAndView display()
	{
		ModelAndView mav=new ModelAndView("Student");
		mav.addObject("stud",repo.findAll());
		return mav;
     }
	/////////////////////////////////////////////////////////////////////////////
	@RequestMapping("/students/new") 
	public ModelAndView createStudent(StudentEntity entity)
	{
		ModelAndView mav=new ModelAndView("createstudent");
		mav.addObject("entity", entity);
		return mav;
		//System.out.println("**************************************************");
		//return "createstudent";
	}
	
	@PostMapping("/submit")
		public String saveStudent(StudentEntity entity)
		{
		repo.save(entity);
		return "redirect:/login";
	}
	
	///////////////////////////////////////////////////////////////////////
	@RequestMapping("/students/edit{id}")
	public ModelAndView editStudent(@PathVariable int id) {
		ModelAndView mav=new ModelAndView("update");
		 StudentEntity std1=repo.findById(id).get();
		 mav.addObject("children", std1);
		 System.out.println("editstudent###########################################");
		 return mav;
	}
	@PostMapping("/students/{id}")
	public String updateStudent(StudentEntity std1)
	{
	repo.save(std1);
	return "redirect:/login";
}
	/////////////////////////////////////////////////////////////////////////
	@RequestMapping("/studentdel/delete{id}")
	public String deleteStudent (@PathVariable int id){
		repo.deleteById(id);
		return "redirect:/login";
	}
	
	/////////////////////////////////////////////////////////////////////////
	
	@ExceptionHandler(ResourceNotFoundException.class)
    public ModelAndView handleResourceNotFoundException(ResourceNotFoundException ex) {
        ModelAndView mav = new ModelAndView("error");
        mav.setStatus(HttpStatus.NOT_FOUND);
        mav.addObject("message", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handleNoHandlerFoundException() {
        ModelAndView mav = new ModelAndView("error");
        mav.setStatus(HttpStatus.NOT_FOUND);
        mav.addObject("message", "404 - Page Not Found");
        return mav;
    }

    static class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException(String message) {
            super(message);
        }
    }

	@Autowired
	Student_repository repo;
	}

